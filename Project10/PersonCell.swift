//
//  PersonCell.swift
//  Project10
//
//  Created by Роман Хоменко on 05.04.2022.
//

import UIKit

class PersonCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    
    
}
